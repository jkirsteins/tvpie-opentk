﻿using System;
using System.Drawing;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;

namespace tvpieopentk
{
	class MainClass
	{
		[STAThread]
		public static void Main()
		{
			
			using (Toolkit.Init ()) {
				Console.WriteLine ("OpenTK.Configuration.RunningOnX11: {0}", OpenTK.Configuration.RunningOnX11);
				Console.WriteLine ("OpenTK.Configuration.RunningOnWindows: {0}", OpenTK.Configuration.RunningOnWindows);
				Console.WriteLine ("OpenTK.Configuration.RunningOnUnix: {0}", OpenTK.Configuration.RunningOnUnix);
				Console.WriteLine ("OpenTK.Configuration.RunningOnSdl2: {0}", OpenTK.Configuration.RunningOnSdl2);
				Console.WriteLine ("OpenTK.Configuration.RunningOnMono: {0}", OpenTK.Configuration.RunningOnMono);
				Console.WriteLine ("OpenTK.Configuration.RunningOnMacOS: {0}", OpenTK.Configuration.RunningOnMacOS);
				Console.WriteLine ("OpenTK.Configuration.RunningOnLinux: {0}", OpenTK.Configuration.RunningOnLinux);
				Console.WriteLine ("OpenTK.Configuration.RunningOnAndroid: {0}", OpenTK.Configuration.RunningOnAndroid);



				Console.WriteLine ("Available resolutions:");
				foreach (var res in OpenTK.DisplayDevice.Default.AvailableResolutions) {
					Console.WriteLine ("- {0}", res.ToString());
				}

				Console.WriteLine ("Resolution: {0}x{1}@{2}", OpenTK.DisplayDevice.Default.Width, OpenTK.DisplayDevice.Default.Height,OpenTK.DisplayDevice.Default.RefreshRate );
				Console.WriteLine ("            {0}", OpenTK.DisplayDevice.Default.ToString());



			}

			try {
				DoApp (GraphicsContextFlags.Embedded);
			} catch (Exception e) {
				Console.Error.WriteLine (e.ToString ());
				DoApp (GraphicsContextFlags.Default);
			} 


			SDL2.SDL.SDL_Quit ();

		}

		static void DoApp(GraphicsContextFlags flags) {
			using (var game = new SimpleES20Window(flags))
			{
				game.Load += (sender, e) =>
				{
					// setup settings, load textures, sounds
					game.VSync = VSyncMode.On;
				};

				// Run the game at 60 updates per second
				game.Run(30.0);
			}
		}
	}
}
