﻿using System;

// This code was written for the OpenTK library and has been released
// to the Public Domain.
// It is provided "as is" without express or implied warranty of any kind.

#region Using Directives

using System.Collections.Generic;
using System.Threading;
using System.Drawing;

using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.ES20;

#endregion

namespace tvpieopentk
{
	public class SimpleES20Window : GameWindow
	{
		#region Constructor
		int programObject = 0;
		DateTime startAt = DateTime.Now;

		const int w = 1280;
		const int h = 720;

		public SimpleES20Window(GraphicsContextFlags flags)
			: base(w, h, GraphicsMode.Default, "", GameWindowFlags.Default, DisplayDevice.Default, 2, 0, flags)
		{
			Console.WriteLine ("SimpleES20Window {0}x{1}, Fullscreen. Flags {2}", w, h, flags);

			string vertexShaderString =
				"attribute vec4 vPosition;    \n" +
				"void main()                  \n" +
				"{                            \n" +
				"   gl_Position = vPosition;  \n" +
				"}                            \n";

			string fragmentShaderString =
				"void main()                                  \n"+
				"{                                            \n"+
				"  gl_FragColor = vec4 ( 1.0, 0.0, 0.0, 1.0 );\n"+
				"}                                            \n";

			int vertexShader;
			int fragmentShader;
			int linked;

			startAt = DateTime.Now;

			vertexShader = LoadShader (ShaderType.VertexShader, vertexShaderString);
			fragmentShader = LoadShader (ShaderType.FragmentShader, fragmentShaderString);

			programObject = GL.CreateProgram ();

			if (programObject == 0) {

				check_gl_error ();
				Console.Error.WriteLine ("Could not create OpenGL program");
				throw new InvalidOperationException ("Could not create OpenGL program");
			}

			GL.AttachShader (programObject, vertexShader);
			GL.AttachShader (programObject, fragmentShader);

			GL.BindAttribLocation (programObject, 0, "vPosition");
			GL.LinkProgram (programObject);

			GL.GetProgram (programObject, GetProgramParameterName.LinkStatus, out linked);

			if (linked != 1) {

				string logInfo = GL.GetProgramInfoLog (programObject);
				Console.Error.WriteLine ("Failure in program linking: {0}", logInfo);
			
				GL.DeleteProgram (programObject);
				throw new InvalidOperationException (string.Format ("Failure in program linking: {0}", logInfo));
			}

			GL.ClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		}

		#endregion

		#region OnLoad

		private void check_gl_error() {
			var err = GL.GetError ();

			while(err!=ErrorCode.NoError) {
				string error;

				switch(err) {
				case ErrorCode.InvalidOperation:      error="INVALID_OPERATION";      break;
				case ErrorCode.InvalidEnum:           error="INVALID_ENUM";           break;
				case ErrorCode.InvalidValue:          error="INVALID_VALUE";          break;
				case ErrorCode.OutOfMemory:          error="OUT_OF_MEMORY";          break;
				case ErrorCode.InvalidFramebufferOperation:  error="INVALID_FRAMEBUFFER_OPERATION";  break;
				default:
					error = string.Format ("Unexpected error code: {0}", err.ToString ());
					break;
				}

				Console.Error.WriteLine ("GL ErrorCode: {0}", error);
				err = GL.GetError ();
			}
		}///

		protected int LoadShader(ShaderType type, string shaderSrc)
		{
			int shader;
			int compiled;

			check_gl_error();

			shader = GL.CreateShader (type);
			check_gl_error();

			if(shader == 0)
			{
				Console.Error.WriteLine ("Could not create OpenGL shader");
				return 0;
			}

			// Load the shader source

			GL.ShaderSource (shader, shaderSrc);

			GL.CompileShader (shader);
			GL.GetShader (shader, ShaderParameter.CompileStatus, out compiled);

			if(compiled != 1)
			{
				string infoLog = GL.GetShaderInfoLog(shader);
				Console.Error.WriteLine ("Error compiling shader: {0}", infoLog);

				GL.DeleteShader (shader);
				return 0;
			}

			return shader;
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			Color4 color = Color4.MidnightBlue;
			GL.ClearColor(color.R, color.G, color.B, color.A);
			GL.Enable(EnableCap.DepthTest);
		}

		#endregion

		#region OnResize

//		/// <summary>
//		/// Called when the user resizes the window.
//		/// </summary>
//		/// <param name="e">Contains the new width/height of the window.</param>
//		/// <remarks>
//		/// You want the OpenGL viewport to match the window. This is the place to do it!
//		/// </remarks>
//		protected override void OnResize(EventArgs e)
//		{
//			GL.Viewport(0, 0, Width, Height);
//		}

		#endregion

		#region OnUpdateFrame

		/// <summary>
		/// Prepares the next frame for rendering.
		/// </summary>
		/// <remarks>
		/// Place your control logic here. This is the place to respond to user input,
		/// update object positions etc.
		/// </remarks>
		protected override void OnUpdateFrame(FrameEventArgs e)
		{
			var keyboard = OpenTK.Input.Keyboard.GetState();
			if (keyboard[OpenTK.Input.Key.Escape])
			{
				this.Exit();
				return;
			}
		

			if (DateTime.Now - startAt > TimeSpan.FromSeconds (5)) {
				this.Exit ();
				return;
			}
		}

		#endregion

		#region OnRenderFrame

		/// <summary>
		/// Place your rendering code here.
		/// </summary>
		protected override void OnRenderFrame(FrameEventArgs e)
		{
			GL.Clear (ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

			float[] vVertices = {
				0.0f, 1.0f, 0.0f,
				0.0f, 0.0f, 0.0f,
				1.0f, 0.0f, 0.0f
			};

			GL.Viewport(0, 0, w, h);

			GL.ColorMask (true, true, true, false);
			GL.Clear (ClearBufferMask.ColorBufferBit);
			GL.UseProgram (programObject);

			GL.VertexAttribPointer<float>(0, 3, VertexAttribPointerType.Float, false, 0, vVertices);
			GL.EnableVertexAttribArray (0);
			GL.DrawArrays (PrimitiveType.Triangles, 0, 3);

			this.SwapBuffers();
		}

		#endregion

		#region private void DrawCube()

		private void DrawCube()
		{
		}

		#endregion
	}
}

